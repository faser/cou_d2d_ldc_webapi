﻿using System.Collections.Generic;

namespace WebApiDemo.Services
{
    public interface IDataService
    {
        List<int> GetData();
    }
}
