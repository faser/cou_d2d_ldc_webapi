﻿namespace WebApiDemo.Services
{
    public interface IScopedTestService
    {
    }

    public class ScopedTestService : IScopedTestService
    {
    }
}
