﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Threading;

namespace WebApiDemo.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class CacheTestController : Controller
    {
        private readonly IMemoryCache cache;

        public CacheTestController(IMemoryCache cahce)
        {
            this.cache = cahce;
        }

        [ResponseCache(Duration = 5)]
        [HttpGet]
        public DateTime ClientCacheTest()
        {
            return DateTime.Now;
        }

        /// <summary>
        /// 判断缓存是否存在
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> TryGetValueCache()
        {
            if (cache.TryGetValue("UserName", out _))
            {
                return "该缓存存在";
            }
            else
            {
                return "该缓存不存在";
            }
        }

        /// <summary>
        /// 写入以及获取缓存
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> GetCache()
        {
            // 写入缓存
            cache.Set("UserName", "admin");
            cache.Set("Password", "12345");

            // 读取缓存
            string userName = cache.Get<string>("UserName");
            string password = cache.Get<string>("Password");

            // 返回
            return userName + "\n" + password;
        }

        /// <summary>
        /// 写入以及删除缓存
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> RemoveCache()
        {
            // 写入缓存
            cache.Set("UserName", "admin");
            cache.Set("Password", "12345");

            // 删除缓存
            cache.Remove("UserName");
            cache.Remove("Password");

            // 返回
            return "缓存删除成功";
        }

        /// <summary>
        /// 缓存过期策略：不过期
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> DefaultCacheTime()
        {
            string msg = string.Empty;
            cache.Set("UserName", "admin");

            // 读取缓存
            for (int i = 1; i <= 5; i++)
            {
                msg += $"第{i}秒缓存值：{cache.Get<string>("UserName")}\n";
                Thread.Sleep(1000);
            }

            // 返回
            return msg;
        }

        /// <summary>
        /// 缓存过期策略：绝对时间过期
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> AbsCacheTime()
        {
            string msg = string.Empty;
            cache.Set("UserName", "admin", TimeSpan.FromSeconds(3));

            // 读取缓存
            for (int i = 1; i <= 5; i++)
            {
                msg += $"第{i}秒缓存值：{cache.Get<string>("UserName")}\n";
                Thread.Sleep(1000);
            }

            // 返回
            return msg;
        }

        /// <summary>
        /// 缓存过期策略：滑动时间过期
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> ScaCacheTime()
        {
            string msg = string.Empty;
            cache.Set("UserName", "admin", new MemoryCacheEntryOptions
            {
                SlidingExpiration = TimeSpan.FromSeconds(3)
            });

            // 读取缓存
            for (int i = 1; i <= 5; i++)
            {
                msg += $"第{i}秒缓存值：{cache.Get<string>("UserName")}\n";
                Thread.Sleep(1000);
            }

            // 返回
            return msg;
        }

        /// <summary>
        /// 缓存过期策略：绝对时间过期+滑动时间过期
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> SpaAndAbsCacheTime()
        {
            string msg = string.Empty;
            cache.Set("UserName", "admin", new MemoryCacheEntryOptions
            {
                SlidingExpiration = TimeSpan.FromSeconds(1.5),
                AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(3)
            });

            // 读取缓存
            for (int i = 1; i <= 5; i++)
            {
                msg += $"第{i}秒缓存值：{cache.Get<string>("UserName")}\n";
                Thread.Sleep(1000);
            }

            // 返回
            return msg;
        }


    }
}
