﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;
using WebApiDemo.Models.Database;
using WebApiDemo.Services;

namespace WebApiDemo.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ServiceTestController : Controller
    {
        private readonly Store_2022Context _db;
        private readonly IDataService _dataService;
        private readonly ISingletonTestService _singletonTestService;
        private readonly ITransientTestService _transientTestService;
        private readonly IScopedTestService _scopedTestService;
        private readonly IJWTService _jwtService;
        public ServiceTestController(Store_2022Context db, IDataService dataService, ISingletonTestService singletonTestService, ITransientTestService transientTestService, IJWTService jwtService)
        {
            _db = db;
            _dataService = dataService;
            _singletonTestService = singletonTestService;
            _transientTestService = transientTestService;
            _jwtService = jwtService;
        }

        [HttpGet]
        public string TestRequest([FromServices] IDataService dataService, [FromServices] IDataService dataService2)
        {
            return dataService.GetHashCode() + "|" + dataService2.GetHashCode();
        }

        /// <summary>
        /// 创建token
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string CreateToken(string name)
        {
            return _jwtService.CreateToken(name);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public string UserInfo()
        {
            //获取用户信息
            return Response.HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name).Value;
        }

    }
}
