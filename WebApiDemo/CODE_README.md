﻿### 准备
设置nuget源：https://api.nuget.org/v3/index.json

### 项目结构

Models
	ApiResultModels/ 存放用于Api返回的数据模型, 子类模型
	Database/ ORM反向生成的数据库模型
	RequestForms/ 客户端的请求表单, 用于接受参数
	UnionModels/ 联合查询的模型, 对数据库内容的抽离或组合, 其中数据需直接可以从查询中赋予. 如需通过代码再次操作其数据(即无法通过一次查询完成), 应归类于ApiResultModels.
