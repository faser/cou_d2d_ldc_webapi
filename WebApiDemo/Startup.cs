using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using NLog.Extensions.Logging;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using WebApiDemo.Models;
using WebApiDemo.Models.Database;
using WebApiDemo.Services;

namespace WebApiDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(logBuilder =>
            {
                logBuilder.ClearProviders();
                logBuilder.AddNLog();
            });

            services.AddMemoryCache();

            services.AddControllers();

            #region JWT Config
            services.Configure<JWTConfig>(Configuration.GetSection("JWTConfig"));

            var tokenConfigs = Configuration.GetSection("JWTConfig").Get<JWTConfig>();

            //Authentication
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenConfigs.Secret)),
                    ValidIssuer = tokenConfigs.Issuer,
                    ValidAudience = tokenConfigs.Audience,
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            #endregion

            //注入数据库上下文
            services.AddDbContext<Store_2022Context>();

            services.Add(new ServiceDescriptor(typeof(IDataService), typeof(DataService), ServiceLifetime.Transient));
            services.AddSingleton<ISingletonTestService, SingletonTestService>();
            services.AddTransient<ITransientTestService, TransientTestService>();
            services.AddScoped<IScopedTestService, ScopedTestService>();
            services.AddScoped<IJWTService, JWTService>();

            //添加cors
            services.AddCors(options =>
            {
                //添加core 策略
                options.AddPolicy("Policy1", //策略名
                    builder =>
                    {
                        builder.WithOrigins("*") //表示可以被所有地址跨域访问 允许跨域访问的地址，不能以正反斜杠结尾。
                            .SetIsOriginAllowedToAllowWildcardSubdomains()//设置策略里的域名允许通配符匹配，但是不包括空。
                                                                          //例：http://localhost:3001 不会被通过
                                                                          // http://xxx.localhost:3001 可以通过
                            .AllowAnyHeader()//配置请求头
                            .AllowAnyMethod();//配置允许任何 HTTP 方法访问
                    });
            });

            //添加swagger
            services.AddSwaggerGen(c =>
            {
                c.CustomSchemaIds(x => x.FullName);
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
                // 获取xml文件名
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                // 获取xml文件路径
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                // 添加控制器层注释，true表示显示控制器注释
                c.IncludeXmlComments(xmlPath, true);

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Description = "在下框中输入请求头中需要添加Jwt授权Token：Bearer Token",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    BearerFormat = "JWT",
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme{
                Reference = new OpenApiReference {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"}
            },new string[] { }
        }
    });

            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            //使用cors，注意中间件的位置位于UseRouting与UseAuthorization之间
            app.UseCors("Policy1");//此处如果填写了app.UserCors("Policy1")，则控制器中默认使用该策略，并且不需要在控制器上添加[EnableCors("Policy1")] 

            app.UseAuthentication(); // 启用认证
            app.UseAuthorization();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "coreMVC3.1");
                c.RoutePrefix = "swagger";     // 路由前置路径, 如果是为空 访问路径就为 根域名/index.html
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
