﻿using System.ComponentModel.DataAnnotations;

namespace WebApiDemo.Models.RequestForms
{
    public class AddGoodsValidRF
    {
        [Required(ErrorMessage = "产品名不能为空")]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        public int CateId { get; set; }

        [Required]
        public string Cover { get; set; }

        [Range(0, 999.99)]
        public decimal Price { get; set; }
        public int Stock { get; set; }
    }
}
