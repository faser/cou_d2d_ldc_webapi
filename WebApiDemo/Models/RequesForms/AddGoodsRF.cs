﻿namespace WebApiDemo.Models.RequestForms
{
    public class AddGoodsRF
    {
        public string Name { get; set; }
        public int CateId { get; set; }
        public string Cover { get; set; }
        public decimal Price { get; set; }
        public int Stock { get; set; }
    }
}
