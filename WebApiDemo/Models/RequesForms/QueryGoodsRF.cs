﻿namespace WebApiDemo.Models.RequesForms
{
    public class QueryGoodsRF
    {
        /// <summary>
        /// 分类Id
        /// </summary>
        public int CateId { get; set; }
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword { get; set; }
    }
}
