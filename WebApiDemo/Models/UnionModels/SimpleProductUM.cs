﻿namespace WebApiDemo.Models.UnionModels
{
    public class SimpleProductUM
    {
        public int GoodsId { get; set; }
        public string Name { get; set; }
    }
}
