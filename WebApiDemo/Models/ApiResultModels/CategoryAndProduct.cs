﻿using System.Collections.Generic;
using WebApiDemo.Models.UnionModels;

namespace WebApiDemo.Models.ApiResultModels
{
    public class CategoryAndProduct
    {
        public string CategoryName { get; set; }
        public int CategoryId { get; set; }
        public List<SimpleProductUM> ProductList { get; set; }
    }
}
